import json

from core import anomaly_detector_application_bridge_pb2 as ad_bridge_pb2


class CreateAnomalyDetectorRequestParser(object):
    def __new__(self, request):
        payload = ad_bridge_pb2.CreateAnomalyDetectorRequest(
            name=request['name'],
            image=request['image']
        )

        if 'version' in request:
            payload.version = request['version']

        for target in request['targets']:
            payload.targets.add(
                source=json.dumps(target['source']),
                strategy=json.dumps(target['strategy'])
            )

        return payload
