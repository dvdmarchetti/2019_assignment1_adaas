from http import HTTPStatus

from google.rpc import code_pb2 as codes


def CanonicalToHttpError(code):
    """
    Map canonical codes to HTTP error codes. This is based
    on the mapping defined by the protobuf HTTP error space.
    """
    if (code == codes.OK):
        return HTTPStatus.OK
    if (code == codes.CANCELLED):
        return HTTPStatus.REQUEST_TIMEOUT
    if (code == codes.UNKNOWN):
        return HTTPStatus.INTERNAL_SERVER_ERROR
    if (code == codes.INVALID_ARGUMENT):
        return HTTPStatus.BAD_REQUEST
    if (code == codes.DEADLINE_EXCEEDED):
        return HTTPStatus.GATEWAY_TIMEOUT
    if (code == codes.NOT_FOUND):
        return HTTPStatus.NOT_FOUND
    if (code == codes.ALREADY_EXISTS):
        return HTTPStatus.CONFLICT
    if (code == codes.PERMISSION_DENIED):
        return HTTPStatus.FORBIDDEN
    if (code == codes.UNAUTHENTICATED):
        return HTTPStatus.UNAUTHORIZED
    if (code == codes.RESOURCE_EXHAUSTED):
        return HTTPStatus.TOO_MANY_REQUESTS
    if (code == codes.FAILED_PRECONDITION):
        return HTTPStatus.BAD_REQUEST
    if (code == codes.ABORTED):
        return HTTPStatus.CONFLICT
    if (code == codes.OUT_OF_RANGE):
        return HTTPStatus.BAD_REQUEST
    if (code == codes.UNIMPLEMENTED):
        return HTTPStatus.NOT_IMPLEMENTED
    if (code == codes.INTERNAL):
        return HTTPStatus.INTERNAL_SERVER_ERROR
    if (code == codes.UNAVAILABLE):
        return HTTPStatus.SERVICE_UNAVAILABLE
    if (code == codes.DATALOSS):
        return HTTPStatus.INTERNAL_SERVER_ERROR
    return HTTPStatus.INTERNAL_SERVER_ERROR


def HttpErrorToCanonical(code):
    """
    Map HTTP error codes to canonical codes. This is based
    on the mapping defined by the protobuf HTTP error space.
    """
    if (code == 400):
        return codes.INVALID_ARGUMENT
    if (code == 403):
        return codes.PERMISSION_DENIED
    if (code == 404):
        return codes.NOT_FOUND
    if (code == 409):
        return codes.ABORTED
    if (code == 416):
        return codes.OUT_OF_RANGE
    if (code == 429):
        return codes.RESOURCE_EXHAUSTED
    if (code == 499):
        return codes.CANCELLED
    if (code == 504):
        return codes.DEADLINE_EXCEEDED
    if (code == 501):
        return codes.UNIMPLEMENTED
    if (code == 503):
        return codes.UNAVAILABLE
    if (code == 401):
        return codes.UNAUTHENTICATED
    if (code >= 200 and code < 300):
        return codes.OK
    if (code >= 400 and code < 500):
        return codes.FAILED_PRECONDITION
    if (code >= 500 and code < 600):
        return codes.INTERNAL
    return codes.UNKNOWN
