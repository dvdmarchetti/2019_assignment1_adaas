from functools import wraps
import json
import os

from flask import request
from jsonschema import Draft7Validator


def validate(definition_filename):
    """
    Load a jsonschema definition and wrap the decorated function/method
    with the validation checking against the specified schema.

    Handles json validation exceptions by returning json errors as response.
    """
    file_path = os.path.join(
        os.path.dirname(os.path.realpath(__file__)),
        "schemas",
        definition_filename + '.json'
    )

    with open(file_path, "r") as read_file:
        schema = json.load(read_file)

    validator = Draft7Validator(schema)

    def wrapper(fn):
        @wraps(fn)
        def wrapped(*args, **kwargs):
            input = request.get_json(force=True)
            errors = [error.message for error in validator.iter_errors(input)]
            if errors:
                return dict(
                    success=False,
                    message="Invalid input",
                    errors=errors,
                    status_code=412
                )
            return fn(*args, **kwargs)
        return wrapped

    return wrapper
