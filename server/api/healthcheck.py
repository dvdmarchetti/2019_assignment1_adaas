from __future__ import print_function

from flask import Blueprint
from flask_restful import Api, Resource


blueprint = Blueprint(
    'Healthcheck',
    __name__,
    url_prefix='/healthcheck'
)
api = Api(blueprint)
maintenance = False


class Healthcheck(Resource):
    def get(self):
        """
        Display a healthcheck message.
        """
        message = ("To toggle maintenance mode, "
                   "make an HTTP request to PATCH /healthcheck")

        if maintenance is True:
            return dict(status="Maintenance mode active", message=message), 503
        return dict(status="Application running", message=message)

    def patch(self):
        """
        Store a newly created resource in storage.
        """
        global maintenance
        maintenance = not maintenance
        return {}, 204


api.add_resource(Healthcheck, '/')
