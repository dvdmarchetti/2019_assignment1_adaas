from __future__ import print_function
import logging

from flask import Blueprint, request
from flask_restful import Api, Resource
from google.protobuf.json_format import MessageToDict
import grpc
from validation.validator import validate

from core import anomaly_detector_application_bridge_pb2 as ad_bridge_pb2
from core import anomaly_detector_application_bridge_pb2_grpc as ad_bridge_pb2_grpc # NOQA
import grpc_utils
import parsers


blueprint = Blueprint(
    'AnomalyDetectors',
    __name__,
    url_prefix='/anomaly-detectors'
)
api = Api(blueprint)

# GRPC_SERVER = 'localhost:50051'
GRPC_SERVER = 'adaas_actuation_bridge:50051'


class AnomalyDetectorList(Resource):
    def get(self):
        """
        Display a listing of the resource.
        """
        with grpc.insecure_channel(GRPC_SERVER) as channel:
            stub = ad_bridge_pb2_grpc.ADApplicationBridgeServiceStub(channel)
            try:
                response = stub.ListAnomalyDetectors(
                    ad_bridge_pb2.ListAnomalyDetectorsRequest()
                )
                return MessageToDict(response)
            except grpc.RpcError as rpc_error:
                logging.error(rpc_error)
                return grpc_utils.abort(rpc_error)

    @validate('anomaly_detector')
    def post(self):
        """
        Store a newly created resource in storage.
        """
        with grpc.insecure_channel(GRPC_SERVER) as channel:
            stub = ad_bridge_pb2_grpc.ADApplicationBridgeServiceStub(channel)
            try:
                response = stub.CreateAnomalyDetector(
                    parsers.CreateAnomalyDetectorRequestParser(
                        request.get_json()
                    )
                )
                return MessageToDict(response), 201
            except grpc.RpcError as rpc_error:
                logging.error(rpc_error)
                return grpc_utils.abort(rpc_error)


class AnomalyDetector(Resource):
    def get(self, anomalyDetector):
        """
        Display the specified resource.

        Parameters
        ----------
        anomalyDetector : string
            The id of the anomalyDetector that will be retrived
        """
        with grpc.insecure_channel(GRPC_SERVER) as channel:
            stub = ad_bridge_pb2_grpc.ADApplicationBridgeServiceStub(channel)
            try:
                response = stub.GetAnomalyDetector(
                    ad_bridge_pb2.GetAnomalyDetectorRequest(id=anomalyDetector)
                )
                return MessageToDict(response)
            except grpc.RpcError as rpc_error:
                logging.error(rpc_error)
                return grpc_utils.abort(rpc_error)

    def put(self, anomalyDetector):
        """
        Update the resource in storage.

        Parameters
        ----------
        anomalyDetector : string
            The id of the anomalyDetector that will be updated
        """
        with grpc.insecure_channel(GRPC_SERVER) as channel:
            ad_bridge_pb2_grpc.ADApplicationBridgeServiceStub(channel)
            try:
                pass
            except grpc.RpcError as rpc_error:
                logging.error(rpc_error)
                return grpc_utils.abort(rpc_error)

    def delete(self, anomalyDetector):
        """
        Remove the specified resource from storage.

        Parameters
        ----------
        anomalyDetector : string
            The id of the anomalyDetector that will be stopped and removed
        """
        with grpc.insecure_channel(GRPC_SERVER) as channel:
            stub = ad_bridge_pb2_grpc.ADApplicationBridgeServiceStub(channel)
            try:
                response = stub.DeleteAnomalyDetector(
                    ad_bridge_pb2.DeleteAnomalyDetectorRequest(
                        id=anomalyDetector
                    )
                )
                return MessageToDict(response), 204
            except grpc.RpcError as rpc_error:
                logging.error(rpc_error)
                return grpc_utils.abort(rpc_error)


api.add_resource(AnomalyDetectorList, '/')
api.add_resource(AnomalyDetector, '/<anomalyDetector>')
