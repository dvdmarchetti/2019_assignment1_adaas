from flask import Flask
from flask_cors import CORS

from api.anomaly_detectors import blueprint as detectors_blueprint
from api.healthcheck import blueprint as healthcheck_blueprint

app = Flask(__name__)
CORS(app)

app.register_blueprint(detectors_blueprint)
app.register_blueprint(healthcheck_blueprint)

if __name__ == "__main__":
    app.run(host='0.0.0.0')
