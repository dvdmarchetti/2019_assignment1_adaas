from google.protobuf.json_format import MessageToDict
from grpc_status import rpc_status

# from core import anomaly_detector_source_drivers_pb2 as ad_source_drivers_pb2
import status_conversions


def abort(rpc_error, code=None):
    """
    Handle RPC Errors by converting gRPC error code to the matching http
    one and outputing a dictonary with error details that can be easily
    converted to JSON.

    Parameters
    ----------
    rpc_error : grpc.Error
        Contains detailed description of the gRPC error that has just happened.

    code : number
        Custom HTTP response code that overrides the converted gRPC error code.
    """
    status = rpc_status.from_call(rpc_error)
    status.code = code or status_conversions.CanonicalToHttpError(status.code)
    return MessageToDict(status), status.code
