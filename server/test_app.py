import flask
import pytest

import app as server


@pytest.fixture
def client():
    server.app.config['TESTING'] = True

    with server.app.test_client() as client:
        yield client


def test_anomaly_detectors(client):
    """Start with a blank database."""

    payload = {
        "name": "cpu-analyzer",
        "image": "adaas-detector:latest"
    }
    response = client.post(
        '/anomaly-detectors',
        json=payload,
        content_type='application/json',
        follow_redirects=True
    )

    # Request should be in JSON
    assert flask.request.is_json
    assert flask.request.get_json() == payload

    # Response should be in JSON
    assert response.status_code == 200
    assert response.is_json
    assert response.get_json()['success'] is False
    assert response.get_json()['status_code'] == 412
