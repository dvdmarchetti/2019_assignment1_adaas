# Assignment 1 - Pipeline implementation
The application used to run the pipeline on is one of the fundamental components of the Anomaly Detection as a Server project.

The pipeline is structured in five layers:

1. **Verify:** to check the code agains PEP 8 style guide.
2. **Test:** executes an example integration-test with pytest.
3. **Package:** builds the docker image of the component and uploads it to GitLab Container Registry tagging it with the sha256 hash of the commit.
4. **Release:** retrives the image packaged in the previous step, tags it accordingly and upload it to the heroku container registry.
5. **Deploy:** deploys the pushed image on the heroku container (this stage has to be triggered manually).
